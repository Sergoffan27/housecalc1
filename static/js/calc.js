        /**
 * Created by Sergey on 18.05.2015.
 */
window.onload = function() {
            var button = document.getElementById("calcButton");
            button.onclick = function () {
                //Чистка элементов, содержащих выходные данные
                //площадь дома
                document.getElementById("s1").innerHTML = null;
                document.getElementById("s2").innerHTML = null;
                document.getElementById("s3").innerHTML = null;
                document.getElementById("s3").innerHTML = null;
                document.getElementById("s4").innerHTML = null;
                document.getElementById("s5").innerHTML = null;
                document.getElementById("s6").innerHTML = null;
                document.getElementById("s7").innerHTML = null;
                //объем утеплителя
                document.getElementById("v1").innerHTML = null;
                document.getElementById("v2").innerHTML = null;
                document.getElementById("v3").innerHTML = null;
                document.getElementById("v4").innerHTML = null;
                document.getElementById("v5").innerHTML = null;
                document.getElementById("v6").innerHTML = null;
                document.getElementById("v24").innerHTML = null;
                //доска 50х150
                document.getElementById("woodFrame").innerHTML=null;
                document.getElementById("woodRoofFrame").innerHTML=null;
                document.getElementById("woodFrameFront").innerHTML=null;
                document.getElementById("woodFloor").innerHTML=null;
                document.getElementById("woodFloor2").innerHTML=null;
                document.getElementById("woodSummary").innerHTML=null;
                //доска 50х100
                document.getElementById("woodInsideWall").innerHTML=null;
                //доска 25х150х6000
                document.getElementById("wood25x150Roof").innerHTML=null;
                //Массив с данными формы
                var b = [];
                b[0] = +document.getElementById("a1").value;
                b[1] = +document.getElementById("a2").value;
                b[2] = +document.getElementById("a3").value;
                b[3] = +document.getElementById("a4").value;
                b[4] = +document.getElementById("a5").value;
                b[5] = +document.getElementById("a6").value;
                b[6] = +document.getElementById("a7").value;
                b[7] = +document.getElementById("u1").value;
                b[8] = +document.getElementById("u2").value;
                b[9] = +document.getElementById("u3").value;
                b[10] = +document.getElementById("u4").value;
                b[11] = +document.getElementById("u5").value;
                b[12] = +document.getElementById("u6").value;
                //Массив с данными Площади дома и др.
                var c = [];
                c[0] = b[0] * b[1] * b[6];
                c[1] = b[0] * b[1];
                c[2] = parseFloat(b[0] * b[3] * 2 + b[1] * b[3] * 2).toFixed(2);
                c[3] = b[3] * b[5];
                c[4] = b[0] * b[4];
                c[5] = parseFloat(b[0] * b[1] * b[6] - (b[5] * b[8] + (2 * b[0] + 2 * b[1]) * b[7]).toFixed(2));
                c[6] = parseFloat(((.5 * b[0] / .866 + .5) * (b[1] + .5) * 2).toFixed(2));
                //Массив с данными для рассчета объема утеплителя
                // и др.
                var a=[];
                a[0]=parseFloat((c[0]*b[9]*b[6]).toFixed(2));
                a[1]=parseFloat((c[2]*b[7]).toFixed(2));
                a[2]=parseFloat((c[3]*b[8]).toFixed(2));
                a[3]=parseFloat((c[6]*b[10]).toFixed(2));
                a[4]=parseFloat((c[4]*b[11]).toFixed(2));
                a[24]=parseFloat((c[2]*b[12]).toFixed(2));
                a[5]=parseFloat((a[0]+a[1]+a[2]+a[3]+a[4]+a[24]).toFixed(2));
                a[6]=c[0];
                a[7]=c[2];
                a[8]=c[0]+c[2];
                a[9]=parseFloat((b[1]/.6*b[3]*2+b[0]/.6*b[3]*2+(3*b[1]+3*b[0])).toFixed(2));
                a[10]=parseFloat(((b[1]+.5)/.6*(.5*b[0]/.866+.5)).toFixed(2));
                a[11]=parseFloat((8*b[4]).toFixed(2));
                a[12]=parseFloat((b[1]/.4*b[0]).toFixed(2));
                a[13]=parseFloat((b[5]/.6).toFixed(2));
                a[14]=parseFloat((a[9]+a[10]+a[11]+2*a[12]).toFixed(2));
                a[15]=parseFloat((b[1]/.6*b[3]*2+b[0]/.6*b[3]*2).toFixed(2));
                a[16]=parseFloat((a[15]+a[10]).toFixed(1));
                a[17]=(c[6]/.9/2).toFixed(0);
                a[18]=c[2]+c[4];
                a[19]=b[5]*b[3]*2+c[2];
                a[20]=c[1];
                a[21]=a[18]+a[19];
                a[22]=c[0]+c[1]+c[2];
                a[23]=c[2]+c[4];

                //Отобразить рассчетные данные на форме
                //Площадь
                document.getElementById("s1").innerHTML += c[0];
                document.getElementById("s2").innerHTML += c[1];
                document.getElementById("s3").innerHTML += c[2];
                document.getElementById("s4").innerHTML += c[3];
                document.getElementById("s5").innerHTML += c[4];
                document.getElementById("s6").innerHTML += c[5];
                document.getElementById("s7").innerHTML += c[6];
                //Объем утеплителя
                document.getElementById("v1").innerHTML+=a[0];
                document.getElementById("v2").innerHTML+=a[1];
                document.getElementById("v3").innerHTML+=a[2];
                document.getElementById("v4").innerHTML+=a[3];
                document.getElementById("v5").innerHTML+=a[4];
                document.getElementById("v6").innerHTML+=a[5];
                document.getElementById("v24").innerHTML+=a[24];
                //доска 50х150
                document.getElementById("woodFrame").innerHTML+=a[9];
                document.getElementById("woodRoofFrame").innerHTML+=a[10];
                document.getElementById("woodFrameFront").innerHTML+=a[11];
                document.getElementById("woodFloor").innerHTML+=a[12];
                document.getElementById("woodFloor2").innerHTML+=a[12];
                document.getElementById("woodSummary").innerHTML+=a[14];
                //доска 50х100
                document.getElementById("woodInsideWall").innerHTML+=a[13];
                //доска 25х150х6000
                document.getElementById("wood25x150Roof").innerHTML+=a[17];
            };
        };